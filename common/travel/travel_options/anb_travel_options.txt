﻿
hire_caver_option = {
	#icon in gfx\interface\icons\travel_options\hire_caver_option.dds is just the "hire_mountaineer_option.dds" as a place holder
	is_shown = {
		is_ruler = yes
	}

	is_valid = {
	}
	
	cost = {
		gold = {
			add = {
				value = high_travel_option_cost
				desc = hire_caver_option
			}
		}
	}

	owner_modifier = {
		mountains_travel_danger = mountains_medium_danger_reduction
		cavern_travel_danger = cavern_medium_danger_reduction
		dwarven_road_travel_danger = dwarven_road_medium_danger_reduction
		dwarven_hold_surface_travel_danger = dwarven_hold_surface_medium_danger_reduction
		dwarven_hold_travel_danger = dwarven_hold_medium_danger_reduction
	}
	
	on_applied_effect = {
		hidden_effect = {
			if = { # No reason to steal too many Mercenaries away for travels; players and AI kings+ only
				limit = {
					OR = {
						is_ai = no
						highest_held_title_tier >= tier_kingdom
					}
				}
				if = { # if civilized get dwarf
					limit = {
						is_civilized_race = yes
					}
					if = {
						limit = {
							exists = root.capital_province
							any_pool_character = {
								province = root.capital_province
								is_available_ai_adult = yes
								has_no_particular_noble_roots_trigger = yes
								is_married = no
								has_trait = deep_terrain_expert
								
								is_race = { RACE = dwarf }
								age >= 30
								age <= 150
							}
						}
						random_pool_character = {
							province = root.capital_province
							limit = {
								is_available_ai_adult = yes
								has_no_particular_noble_roots_trigger = yes
								is_married = no
								has_trait = deep_terrain_expert
								
								is_race = { RACE = dwarf }
								age >= 30
								age <= 150
							}
							save_scope_as = new_guide
						}
					}
					else = {
						create_character = {
							location = root.location
							template = caver_guide_dwarf
							save_scope_as = new_guide
						}
					}
				}
				else = { # if monstrous get kobold or goblin
					if = {
						limit = {
							exists = root.capital_province
							any_pool_character = {
								province = root.capital_province
								is_available_ai_adult = yes
								has_no_particular_noble_roots_trigger = yes
								is_married = no
								has_trait = deep_terrain_expert
								
								trigger_if = { # kobolds want kobolds
									limit = {
										ROOT = { is_kobold_culture = yes }
									}
									is_race = { RACE = kobold }
								}
								# trigger_else_if = { # goblins want goblins
									# limit = {
										# ROOT = { is_goblin_culture = yes }
									# }
									# is_race = { RACE = goblin }
								# }
								trigger_else = {
									OR = {
										is_race = { RACE = kobold }
										is_race = { RACE = goblin }
									}
								}
								age >= 30
								age <= 50
							}
						}
						random_pool_character = {
							province = root.capital_province
							limit = {
								is_available_ai_adult = yes
								has_no_particular_noble_roots_trigger = yes
								is_married = no
								has_trait = deep_terrain_expert
								
								trigger_if = { # kobolds want kobolds
									limit = {
										ROOT = { is_kobold_culture = yes }
									}
									is_race = { RACE = kobold }
								}
								# trigger_else_if = { # goblins want goblins
									# limit = {
										# ROOT = { is_goblin_culture = yes }
									# }
									# is_race = { RACE = goblin }
								# }
								trigger_else = {
									OR = {
										is_race = { RACE = kobold }
										is_race = { RACE = goblin }
									}
								}
								age >= 30
								age <= 50
							}
							save_scope_as = new_guide
						}
					}
					else = {
						if = { # if kobold, get kobold
							limit = {
								is_kobold_culture = yes
							}
							create_character = {
								location = root.location
								template = caver_guide_kobold
								save_scope_as = new_guide
							}
						}
						# else_if = { # if goblin, get goblin # Anbennar TODO: implement when goblins are added
							# limit = {
								# is_goblin_culture = yes
							# }
							# create_character = {
								# location = root.location
								# template = caver_guide_goblin
								# save_scope_as = new_guide
							# }
						# }
						else = { # if neither kobold nor goblin get one at random
							random_list = {
								1 = {
									create_character = {
										location = root.location
										template = caver_guide_kobold
										save_scope_as = new_guide
									}
								}
								# 1 = { # Anbennar TODO: implement when goblins are added
									# create_character = {
										# location = root.location
										# template = caver_guide_goblin
										# save_scope_as = new_guide
									# }
								# }
							}
						}
					}
				}
				
				if = { # notification 
					limit = {
						exists = scope:new_guide
					}
					scope:new_guide = {
						add_gold = root.low_travel_option_cost
						move_to_pool = yes
						add_character_flag = travel_option_added_character	#to send it back to pool when the travel is done
					}
					send_interface_toast = {
						title = caver_recruited.t
						left_icon = scope:new_guide
						root.current_travel_plan ?= {
							add_companion = scope:new_guide
						}
					}
				}
			}
		}
		custom_tooltip = caver_recruited.tt
	}
	
	ai_will_do = {
		value = 0
		if = {
			limit = {
				any_future_path_location = {
					count >= 3
					OR = {
						terrain = mountains
						terrain = dwarven_road
						terrain = cavern
						terrain = dwarven_hold_surface
						terrain = dwarven_hold
					}
				}
			}
			add = {
				value = 100
			}
		}
	}
}
