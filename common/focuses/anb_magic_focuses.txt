﻿magic_duelist_focus = {
	lifestyle = magic_lifestyle
	
	modifier = {
		prowess = 3
	}

	desc = {
		desc = magic_duelist_focus_desc
		desc = line_break
	}

	auto_selection_weight = {
		value = 11
		if = {
			limit = {
				has_magical_affinity_trigger = yes
			}
			add = 1989
		}
		if = {
			limit = {
				has_trait = brave
			}
			multiply = 5
		}
	}
}
