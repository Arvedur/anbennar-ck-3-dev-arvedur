﻿# Anbennar: keep these until we remove every mention in the code

heritage_akan = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = mena
}

heritage_arabic = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = mena
}

heritage_baltic = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_balto_finnic = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_berber = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = mena
}

heritage_brythonic = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_burman = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_byzantine = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_central_african = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = mena
}

heritage_central_germanic = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_chinese = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_dravidian = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_east_african = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = mena
}

heritage_east_slavic = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_frankish = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_goidelic = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_iberian = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_indo_aryan = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_iranian = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_israelite = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_latin = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_magyar = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_mongolic = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_north_germanic = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_qiangic = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_sahelian = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_senegambian = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_somalian = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = mena
}

heritage_bantu = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = mena
}

heritage_south_slavic = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_tibetan = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_turkic = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_ugro_permian = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_volga_finnic = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_west_african = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = mena
}

heritage_west_germanic = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_west_slavic = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_yoruba = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = mena
}

heritage_tocharian = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_vlach = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_caucasian = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_syriac = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = mena #TODO_CD_EP3 did we get a new set of byzantine audio to add to related heritages?
}

heritage_gothic = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_egyptian = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = mena
}

heritage_hunnic = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_ancient_greek = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}

heritage_albanian = {
	type = heritage
	is_shown = {
		always = no
	}
	audio_parameter = european
}
