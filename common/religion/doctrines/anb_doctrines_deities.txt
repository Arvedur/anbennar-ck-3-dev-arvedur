﻿doctrine_patron_deity = {
	group = "main_group"
	is_available_on_create = {
		religion_tag = cannorian_pantheon_religion
	}
	
	doctrine_patron_deity_castellos = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_castellos }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_castellos = 0
			hostility_override_doctrine_patron_deity_castellos_esmaryal_falah = 0
			hostility_override_doctrine_patron_deity_castellos_falah_nerat = 0
			hostility_override_doctrine_patron_deity_south_castellos = 2
		}
	}
	
	doctrine_patron_deity_the_dame = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_the_dame }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_the_dame = 0
			hostility_override_doctrine_patron_deity_the_dame_munas = 0
			hostility_override_doctrine_patron_deity_the_dame_adean = 0
			hostility_override_doctrine_patron_deity_ara_the_dame = 0
			hostility_override_doctrine_patron_deity_esmaryal_the_dame_ara = 0
		}
	}
	
	doctrine_patron_deity_esmaryal = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_esmaryal }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_esmaryal = 0
			hostility_override_doctrine_patron_deity_esmaryal_the_dame_ara = 0
			hostility_override_doctrine_patron_deity_castellos_esmaryal_falah = 0
		}
	}
	
	doctrine_patron_deity_falah = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_falah }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_falah = 0
			hostility_override_doctrine_patron_deity_castellos_esmaryal_falah = 0
			hostility_override_doctrine_patron_deity_castellos_falah_nerat = 0
		}
	}
	
	doctrine_patron_deity_nerat = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_nerat }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_nerat = 0
			hostility_override_doctrine_patron_deity_castellos_falah_nerat = 0
		}
	}
	
	doctrine_patron_deity_adean = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_adean }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_adean = 0
			hostility_override_doctrine_patron_deity_the_dame_adean = 0
			hostility_override_doctrine_patron_deity_adean_minara = 0
		}
	}
	
	doctrine_patron_deity_ryala = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_ryala }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_ryala = 0
		}
	}
	
	doctrine_patron_deity_balgar = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_balgar }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_balgar = 0 #maybe also for dwarven ancestor worship
		}
	}
	
	doctrine_patron_deity_ara = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_ara }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_ara = 0
			hostility_override_doctrine_patron_deity_ara_the_dame = 0
			hostility_override_doctrine_patron_deity_esmaryal_the_dame_ara = 0
		}
	}
	
	doctrine_patron_deity_munas = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_munas }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_munas = 0
			hostility_override_doctrine_patron_deity_the_dame_munas = 0
			hostility_override_doctrine_patron_deity_uelos = 2
		}
	}
	
	doctrine_patron_deity_nathalyne = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_nathalyne }
				multiply = 0
			}
		}
		parameters = {
			 hostility_override_doctrine_patron_deity_nathalyne = 0
		}
	}
	
	doctrine_patron_deity_begga = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_begga }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_begga = 0
		}
	}
	
	doctrine_patron_deity_minara = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_minara }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_minara = 0
			hostility_override_doctrine_patron_deity_adean_minara = 0
		}
	}
	
	doctrine_patron_deity_the_dame_munas = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_the_dame_munas }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_the_dame = 0
			hostility_override_doctrine_patron_deity_munas = 0
			hostility_override_doctrine_patron_deity_the_dame_munas = 0
			hostility_override_doctrine_patron_deity_the_dame_adean = 0
			hostility_override_doctrine_patron_deity_ara_the_dame = 2 #Temple of the Highest Moon views Tefori damish as hostile
			hostility_override_doctrine_patron_deity_esmaryal_the_dame_ara = 0
			hostility_override_doctrine_patron_deity_uelos = 2
		}
	}
	
	doctrine_patron_deity_the_dame_adean = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_the_dame_adean }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_the_dame = 0
			hostility_override_doctrine_patron_deity_adean = 0
			hostility_override_doctrine_patron_deity_the_dame_adean = 0
			hostility_override_doctrine_patron_deity_the_dame_munas = 0
			hostility_override_doctrine_patron_deity_ara_the_dame = 0
			hostility_override_doctrine_patron_deity_esmaryal_the_dame_ara = 0
			hostility_override_doctrine_patron_deity_adean_minara = 0
		}
	}
	
	doctrine_patron_deity_ara_the_dame = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_ara_the_dame }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_ara = 0
			hostility_override_doctrine_patron_deity_the_dame = 0
			hostility_override_doctrine_patron_deity_ara_the_dame = 0
			hostility_override_doctrine_patron_deity_the_dame_munas = 2 #Viewed as hostile by Temple of the Highest Moon
			hostility_override_doctrine_patron_deity_the_dame_adean = 0
			hostility_override_doctrine_patron_deity_esmaryal_the_dame_ara = 0
		}
	}
	
	doctrine_patron_deity_esmaryal_the_dame_ara = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_esmaryal_the_dame_ara }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_esmaryal = 0
			hostility_override_doctrine_patron_deity_the_dame = 0
			hostility_override_doctrine_patron_deity_ara = 0
			hostility_override_doctrine_patron_deity_esmaryal_the_dame_ara = 0
			hostility_override_doctrine_patron_deity_the_dame_munas = 0
			hostility_override_doctrine_patron_deity_the_dame_adean = 0
			hostility_override_doctrine_patron_deity_ara_the_dame = 0
			hostility_override_doctrine_patron_deity_castellos_esmaryal_falah = 0
		}
	}
	
	doctrine_patron_deity_adean_minara = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_adean_minara }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_adean = 0
			hostility_override_doctrine_patron_deity_minara = 0
			hostility_override_doctrine_patron_deity_adean_minara = 0
			hostility_override_doctrine_patron_deity_the_dame_adean = 0
		}
	}
	
	doctrine_patron_deity_castellos_esmaryal_falah = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_castellos_esmaryal_falah }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_castellos = 0
			hostility_override_doctrine_patron_deity_esmaryal = 0
			hostility_override_doctrine_patron_deity_falah = 0
			hostility_override_doctrine_patron_deity_castellos_esmaryal_falah = 0
			hostility_override_doctrine_patron_deity_esmaryal_the_dame_ara = 0
			hostility_override_doctrine_patron_deity_castellos_falah_nerat = 0
			hostility_override_doctrine_patron_deity_south_castellos = 2
		}
	}
	
	doctrine_patron_deity_castellos_falah_nerat = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_castellos_falah_nerat }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_castellos = 0
			hostility_override_doctrine_patron_deity_falah = 0
			hostility_override_doctrine_patron_deity_nerat = 0
			hostility_override_doctrine_patron_deity_castellos_falah_nerat = 0
			hostility_override_doctrine_patron_deity_castellos_esmaryal_falah = 0
			hostility_override_doctrine_patron_deity_south_castellos = 2
		}
	}
	
	doctrine_patron_deity_south_castellos = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_south_castellos }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_castellos = 2 #Hostile to northern gods
			hostility_override_doctrine_patron_deity_south_castellos = 0
			hostility_override_doctrine_patron_deity_castellos_falah_nerat = 2 #Hostile to northern gods
			hostility_override_doctrine_patron_deity_castellos_esmaryal_falah = 2 #Hostile to northern gods
		}
	}
	
	doctrine_patron_deity_uelos = {
		can_pick = {
			has_doctrine = tenet_primordial_of_the_seas
		}
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_uelos }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_uelos = 0
			hostility_override_doctrine_patron_deity_munas = 2
			hostility_override_doctrine_patron_deity_the_dame_munas = 2
		}
	}
	
	doctrine_patron_deity_wip = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_patron_deity_wip }
				multiply = 0
			}
		}
		parameters = {
			hostility_override_doctrine_patron_deity_wip = 0
			hostility_override_faithless_hostility_doctrine = 0 #TODO - Change this after I implement unique gnome tenet
		}
	}
}