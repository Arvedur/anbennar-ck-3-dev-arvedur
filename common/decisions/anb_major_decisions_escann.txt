﻿anb_restore_general_trystans_kingdom_decision = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_realm.dds"
	}
	decision_group_type = major
	ai_goal = yes

	cost = {
		gold = 200
	}

	is_shown = {
		is_landed = yes
		culture = culture:trystanite
		capital_province = {
			geographical_region = custom_trystans_kingdom
		}
		primary_title.tier <= tier_kingdom
		NOT = {
			is_target_in_global_variable_list = {
				name = unavailable_unique_decisions
				target = flag:flag_formed_trystans_kingdom
			}
		}
	}

	is_valid_showing_failures_only = {
		completely_controls = title:d_burnoll
		completely_controls = title:d_humacvord
		completely_controls = title:d_medirleigh
	}

	effect = {
		primary_title = {
			save_scope_as = old_title
		}
		title:k_trystans_kingdom = {
			set_de_jure_liege_title = scope:old_title.de_jure_liege
		}
		add_character_modifier = anb_trystan_come_again_modifier
		add_to_global_variable_list = {
			name = unavailable_unique_decisions
			target = flag:flag_formed_trystans_kingdom
		}
		create_title_and_vassal_change = {
			type = created
			save_scope_as = change
			add_claim_on_loss = no
		}
		title:k_trystans_kingdom = {
			change_title_holder = {
				holder = root
				change = scope:change
			}
		}
		resolve_title_and_vassal_change = scope:change
		title:d_burnoll = {
			set_de_jure_liege_title = title:k_trystans_kingdom
		}
		title:d_humacvord = {
			set_de_jure_liege_title = title:k_trystans_kingdom
		}
		title:d_medirleigh = {
			set_de_jure_liege_title = title:k_trystans_kingdom
		}
		if = {
			limit = {
				has_title = title:d_wallor
			}
			title:d_wallor = {
				set_de_jure_liege_title = title:k_trystans_kingdom
			}
		}
		else = {
			title:d_wallor = {
				set_de_jure_liege_title = title:d_southgate.de_jure_liege
			}
		}
		if = {
			limit = {
				has_title = title:d_themin
			}
			title:d_themin = {
				set_de_jure_liege_title = title:k_trystans_kingdom
			}
		}
		else = {
			title:d_themin = {
				set_de_jure_liege_title = title:d_blademarch.de_jure_liege
			}
		}
		if = {
			limit = { primary_title.tier <= tier_kingdom }
			set_primary_title_to = title:k_trystans_kingdom
		}
		create_legend_seed = {
			type = heroic
			quality = famed
			chronicle = new_title
			properties = {
				title = title:k_trystans_kingdom
				founder = root
			}
		}
	}

	ai_potential = {
		always = yes
	}
	ai_will_do = {
		base = 100
	}
}
anb_masters_of_humacfeld_decision = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_realm.dds"
	}
	decision_group_type = major
	ai_goal = yes

	cost = {
		gold = 300
		prestige = 300
	}

	is_shown = {
		is_landed = yes
		OR = {
			culture = culture:trystanite
			culture = culture:castanorian
			culture = culture:marcher
		}
		capital_province = {
			geographical_region = custom_humacfeld
		}
		NOT = {
			is_target_in_global_variable_list = {
				name = unavailable_unique_decisions
				target = flag:flag_masters_of_humacfeld_decision
			}
		}
	}

	is_valid_showing_failures_only = {
		has_title = title:k_humacfeld
		completely_controls = title:k_humacfeld
	}

	effect = {
		set_culture = culture:humacfelder
		capital_county = {
			set_county_culture = culture:humacfelder
		}
		if = {
			limit = {
				any_child = {
					liege = root
					culture != culture:humacfelder
				}
			}
			custom_tooltip = anb_masters_of_humacfeld_decision_children_convert_tt
			hidden_effect = {
				every_child = {
					limit = {
						liege = root
					}
					set_culture = culture:humacfelder
				}
			}
		}
		custom_tooltip = anb_masters_of_humacfeld_decision_75_counties_convert_tt
		hidden_effect = {
			ordered_county_in_region = {
				region = custom_humacfeld
				limit = {
					NOT = {
						culture = culture:humacfelder
					}
					this = root.capital_county
				}
				max = anb_humacfeld_value
				set_county_culture = culture:humacfelder
				if = {
					limit = {
						NOT = {
							holder.culture = culture:humacfelder
						}
						holder = {
							opinion = {
								target = root
								value >= 25
							}
						}
					}
					holder = { set_culture = culture:humacfelder }
				}
			}
		}
		add_character_modifier = anb_uniter_of_humacfeld_modifier
		house = {
			add_house_modifier = { modifier = anb_rulers_of_humacfeld_house_modifier years = 100 }
		}
		add_to_global_variable_list = {
			name = unavailable_unique_decisions
			target = flag:flag_masters_of_humacfeld_decision
		}
		trigger_event = anb_decision_major_events.0100 
	}

	ai_potential = {
		always = yes
	}
	ai_will_do = {
		base = 100
	}
}