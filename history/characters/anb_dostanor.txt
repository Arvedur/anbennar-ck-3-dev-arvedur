﻿
korbarid_0008 = {
	name = "Bogan"
	dna = dna_bogan_aldanid
	dynasty = dynasty_aldanid	#Aldanid
	religion = "korbarid_dragon_cult"
	culture = "korbarid"

	trait = education_martial_4
	trait = brave
	trait = arrogant
	trait = zealous
	trait = scaly
	trait = giant
	trait = strong
	trait = holy_warrior
	trait = aggressive_attacker
	trait = race_human
	
	father = korbarid_0013 #Moskon Aldanid
	mother = korbarid_0014 #Zalmosă Eskrumbnid

	982.6.11 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	
	998.12.20 = {
		add_spouse = korbarid_0009 #Giramăr of Agresasdiel
	}

	996.10.31 = {
		effect = {
			set_relation_soulmate = character:korbarid_0009 #Giramăr of Agresasdiel
		}
	}
}

korbarid_0009 = {
	name = "GiramA_r"
	dna = dna_giramar_agresasdiel
	religion = "korbarid_dragon_cult"
	culture = "korbarid"
	female = yes

	trait = education_stewardship_2
	trait = race_human
	trait = diligent
	trait = calm
	trait = zealous
	trait = shrewd
	trait = magical_affinity_1

	973.3.30 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}	
}

korbarid_0010 = {
	name = "DapI_"
	dynasty = dynasty_aldanid	#Aldanid
	religion = "korbarid_dragon_cult"
	culture = "korbarid"
	
	trait = race_human
	trait = twin
	trait = pensive
	trait = shy
	
	father = korbarid_0008 #Bogan Alanid
	mother = korbarid_0009 #Giramăr of Agresasdiel
	
	1010.5.7 = {
		birth = yes
	}
}

korbarid_0011 = {
	name = "Dotos"
	dynasty = dynasty_aldanid	#Aldanid
	religion = "korbarid_dragon_cult"
	culture = "korbarid"
	
	trait = race_human
	trait = twin
	trait = bossy
	trait = wrathful
	father = korbarid_0008 #Bogan Alanid
	mother = korbarid_0009 #Giramăr of Agresasdiel
	
	1010.5.7 = {
		birth = yes
	}
}

korbarid_0012 = {
	name = "S_aper"
	dynasty = dynasty_aldanid	#Aldanid
	religion = "korbarid_dragon_cult"
	culture = "korbarid"
	female = yes
	
	trait = race_human
	trait = education_intrigue_3
	trait = deceitful
	trait = patient
	trait =fickle
	
	father = korbarid_0008 #Bogan Alanid
	mother = korbarid_0009 #Giramăr of Agresasdiel

	1000.12.12 = {
		birth = yes
	}
}

korbarid_0013 = {
	name = "Moskon"
	dynasty = dynasty_aldanid	#Aldanid
	religion = "korbarid_dragon_cult"
	culture = "korbarid"
	
	trait = race_human
	trait = education_intrigue_1
	trait = vengeful
	trait = cynical
	trait = content

	926.10.10 = {
		birth = yes
	}
	
	998.4.3 = {
		death = {
			death_reason = death_natural_causes
		}
	}
	952.8.9 = {
		add_spouse = korbarid_0014 #Zalmosă Eskrumbnid
	}

}

korbarid_0014 = {
	name = "ZalmosA_"
	dynasty = dynasty_eskrumbnid	#Eskrumbnid
	religion = "korbarid_dragon_cult"
	culture = "korbarid"
	female = yes
	
	trait = race_human
	trait = education_diplomacy_2
	trait = generous
	trait = gregarious
	trait = gluttonous

	937.8.29 = {
		birth = yes
	}
	
	982.6.11 = {
		death = {
			death_reason = death_childbirth
		}
	}
}

korbarid_0015 = {
	name = "Gebucu"
	dynasty = dynasty_aldanid	#Aldanid
	religion = "korbarid_dragon_cult"
	culture = "korbarid"

	trait = education_learning_2
	trait = trusting
	trait = humble
	trait = lazy
	trait = race_human

	father = korbarid_0013 #Moskon Aldanid
	mother = korbarid_0014 #Zalmosă Eskrumbnid
	
	979.1.12 = {
		birth = yes
	}
	998.9.20 = {
		death = {
			death_reason = death_hunting_mysterious
		}
	}
}

korbarid_0016 = {
	name = "Carnabon"
	dna = dna_carnabon_buzasnid
	dynasty = dynasty_buzasnid	#Buzasnid
	religion = "korbarid_dragon_cult"
	culture = "korbarid"

	trait = education_diplomacy_2
	trait = trusting
	trait = gregarious
	trait = eccentric
	trait = lifestyle_traveler
	trait = race_human
	
	father = korbarid_0018	#Dorpokis Buzasnid
	mother = korbarid_0019	#Nataporă Karnid
	
	1003.4.19 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	
	1019.10.15 = {
		add_spouse = korbarid_0012 #Șaper Aldanid
	}

	1016.4.19 = {
		effect = {
			set_relation_best_friend = character:korbarid_0017 #Estejar Buzasnid
		}
	}
	
	1019.7.28 = {
		effect = {
			add_pressed_claim = title:c_corargin
		}
	}
}

korbarid_0017 = {
	name = "Estejar"
	dna = dna_estejar_buzasnid
	dynasty = dynasty_buzasnid	#Buzasnid
	religion = "korbarid_dragon_cult"
	culture = "korbarid"

	trait = education_martial_3
	trait = race_human
	trait = brave
	trait = patient
	trait = compassionate
	trait = strong
	trait = one_legged
	trait = one_eyed
	trait = adventurer
	trait = scarred
	trait = unyielding_defender
	trait = rough_terrain_expert
	
	father = korbarid_0023 #Burebistan Buzasnid
	mother = korbarid_0024 #Dadă Eskrumbnid

	955.2.25 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}	
}

korbarid_0018 = {
	name = "Dorpokis"
	dynasty = dynasty_buzasnid	#Buzasnid
	religion = "korbarid_dragon_cult"
	culture = "korbarid"
	
	trait = race_human
	trait = generous
	trait = arbitrary
	trait = stubborn
	trait = education_stewardship_3
	
	father = korbarid_0023 #Burebistan Buzasnid
	mother = korbarid_0024 #Dadă Eskrumbnid
	
	960.12.30 = {
		birth = yes
	}

	1019.7.28 = {
		death = {
			death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

korbarid_0019 = {
	name = "NataporA_"
	dynasty = dynasty_karnid	#Karnid
	religion = "korbarid_dragon_cult"
	culture = "korbarid"
	female = yes
	
	trait = race_human
	trait = cynical
	trait = content
	trait = shy
	trait = depressed_1
	trait = education_stewardship_3
	
	father = korbarid_0025	#Zolatur Karnid
	mother = korbarid_0026	#Rigoză, Kolvan’s Mom
	
	965.4.22 = {
		birth = yes
	}

	1020.8.23 = {
		death = {
			death_reason = death_depressed
		}
	}
}

korbarid_0020 = {
	name = "GebelizI_s"
	dynasty = dynasty_buzasnid	#Buzasnid
	religion = "korbarid_dragon_cult"
	culture = "korbarid"
	
	trait = race_human
	trait = bossy
	trait = ill
	
	father = korbarid_0018	#Dorpokis Buzasnid
	mother = korbarid_0019	#Nataporă Karnid
	
	987.7.30 = {
		birth = yes
	}

	1000.4.25 = {
		death = {
			death_reason = death_ill
		}
	}
}

korbarid_0021 = {
	name = "Mucador"
	dynasty = dynasty_buzasnid	#Buzasnid
	religion = "korbarid_dragon_cult"
	culture = "korbarid"
	
	trait = race_human
	trait = impatient
	trait = stubborn
	trait = callous
	trait = education_martial_1
	
	father = korbarid_0018	#Dorpokis Buzasnid
	mother = korbarid_0019	#Nataporă Karnid

	990.10.15 = {
		birth = yes
	}

	1014.9.20 = {
		death = {
			death_reason = death_horse_riding_accident
		}
	}
}

korbarid_0022 = {
	name = "Sermamer"
	dynasty = dynasty_buzasnid	#Buzasnid
	religion = "korbarid_dragon_cult"
	culture = "korbarid"
	
	trait = race_human
	trait = brave
	trait = just
	trait = lazy
	trait = education_martial_3
	trait = lifestyle_hunter
	
	father = korbarid_0018	#Dorpokis Buzasnid
	mother = korbarid_0019	#Nataporă Karnid

	990.10.15 = {
		birth = yes
	}

	1014.9.20 = {
		death = {
			death_reason = death_deer
		}
	}
}


korbarid_0023 = {
	name = "Burebistan"
	dynasty = dynasty_buzasnid	#Buzasnid
	religion = "korbarid_dragon_cult"
	culture = "korbarid"
	
	trait = race_human
	trait = shy
	trait = zealous
	trait = diligent
	trait = education_learning_4

	920.2.7 = {
		birth = yes
	}

	988.8.8 = {
		death = {
			death_reason = death_natural_causes
		}
	}
}

korbarid_0024 = {
	name = "DadA_"
	dynasty = dynasty_eskrumbnid	#Eskrumbnid
	religion = "korbarid_dragon_cult"
	culture = "korbarid"
	female = yes

	trait = race_human
	trait = gregarious
	trait = cynical
	trait = lazy
	trait = education_diplomacy_1

	918.5.25 = {
		birth = yes
	}

	990.6.28 = {
		death = {
			death_reason = death_natural_causes
		}
	}
}

korbarid_0025 = {
	name = "Zolatur"
	dynasty = dynasty_karnid	#Karnid
	religion = "korbarid_dragon_cult"
	culture = "korbarid"
	
	trait = race_human
	trait = fickle
	trait = greedy
	trait = lazy
	trait = education_stewardship_2
	
	930.1.31 = {
		birth = yes
	}

	985.5.25 = {
		death = {
			death_reason = death_natural_causes
		}
	}
}

korbarid_0026 = {
	name = "RigozA_"
	religion = "korbarid_dragon_cult"
	culture = "korbarid"
	female = yes
	
	trait = race_human
	trait = temperate
	trait = humble
	trait = arbitrary
	trait = education_learning_3
	
	936.1.27 = {
		birth = yes
	}

	1117.10.23 = {
		death = {
			death_reason = death_natural_causes
		}
	}
}

corvurian_0008 = {
	name = "Alveru"
	dna = dna_alveru_gahtalden
	dynasty = dynasty_gahtalden	#Gahtalden
	religion = "castanorian_pantheon"
	culture = "corvurian"

	trait = education_martial_3
	trait = vengeful
	trait = wrathful
	trait = impatient
	trait = irritable
	trait = lifestyle_blademaster
	trait = reckless
	trait = race_human
	
	father = corvurian_0010 #Ceciliu Gahtalden
	mother = corvurian_0009 #Constanta Geskaivasu

	1005.3.20 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}

	1012.2.8 = {
		effect = {
			set_relation_rival = character:korbarid_0008 #Bogan Aldanid
		}
	}
}

corvurian_0009 = {
	name = "Constanta"
	dna = dna_constanta_geskaivasu
	dynasty = dynasty_geskaivasu	#Geskaivasu
	religion = "castanorian_pantheon"
	culture = "corvurian"
	female = yes

	trait = education_intrigue_4
	trait = race_human
	trait = vengeful
	trait = deceitful
	trait = zealous

	969.7.18 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	
	987.1.1 = {
		add_spouse = corvurian_0010 #Ceciliu Gahtalden
	}
	
	1012.2.6 = {
		effect = {
			set_relation_rival = character:korbarid_0008 #Bogan Aldanid
		}
	}	
}

corvurian_0010 = {
	name = "Ceciliu"
	dynasty = dynasty_gahtalden	#Gahtalden
	religion = "castanorian_pantheon"
	culture = "corvurian"
	
	trait = race_human
	trait = education_learning_2
	trait = trusting
	trait = just
	trait = honest
	trait = physique_good_2

	965.4.15 = {
		birth = yes
	}
	
	1012.2.6 = {
		death = {
				death_reason = death_duel
				killer = korbarid_0008 #Bogan Aldanid
		}
	}
}

corvurian_0011 = {
	name = "Edmond"
	dynasty = dynasty_gahtalden	#Gahtalden
	religion = "castanorian_pantheon"
	culture = "corvurian"
	
	trait = race_human
	trait = education_martial_2
	trait = brave
	trait = stubborn
	trait = diligent
	
	father = corvurian_0010 #Ceciliu Gahtalden
	mother = corvurian_0009 #Constanta Geskaivasu

	988.2.26 = {
		birth = yes
	}
	
	1012.2.7 = {
		death = {
				death_reason = death_duel
				killer = korbarid_0008 #Bogan Aldanid
		}
	}
}

corvurian_0012 = {
	name = "Gelmac"
	dynasty = dynasty_gahtalden	#Gahtalden
	religion = "castanorian_pantheon"
	culture = "corvurian"
	
	trait = race_human
	trait = education_stewardship_3
	trait = impatient
	trait = vengeful
	trait = ambitious
	
	father = corvurian_0010 #Ceciliu Gahtalden
	mother = corvurian_0009 #Constanta Geskaivasu

	990.8.29 = {
		birth = yes
	}
	
	1012.2.8 = {
		death = {
				death_reason = death_duel
				killer = korbarid_0008 #Bogan Aldanid
		}
	}
}

corvurian_0013 = {
	name = "Sandr"
	dynasty = dynasty_gahtalden	#Gahtalden
	religion = "castanorian_pantheon"
	culture = "corvurian"
	
	trait = race_human
	trait = education_martial_3
	trait = arrogant
	trait = fickle
	trait = wrathful
	
	father = corvurian_0010 #Ceciliu Gahtalden
	mother = corvurian_0009 #Constanta Geskaivasu

	993.8.29 = {
		birth = yes
	}
	
	1012.2.9 = {
		death = {
				death_reason = death_duel
				killer = korbarid_0008 #Bogan Aldanid
		}
	}
}

corvurian_0014 = {
	name = "Tisiru"
	dna = dna_tisiru_zenanid
	dynasty = dynasty_zenanid #Zenanid
	religion = "castanorian_pantheon"
	culture = "corvurian"

	trait = education_intrigue_2
	trait = sadistic
	trait = deceitful
	trait = greedy
	trait = torturer
	trait = disloyal
	trait = race_human

	989.7.29 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	
	1000.1.1 = {
		effect = {
			add_secret = { type = secret_cannibal }
		}
	}
}

corvurian_0015 = {
	name = "Azilina"
	dna = dna_azilina_sil_mendruta
	dynasty = dynasty_sil_mendruta	#síl Mendruta
	religion = "castanorian_pantheon"
	culture = "corvurian"
	female = yes

	trait = education_diplomacy_2
	trait = gregarious
	trait = honest
	trait = impatient
	trait = race_human
	
	father = corvurian_0018 #Rațibidan Mendrutanid
	mother = corvurian_0019 #Zirașes Gujirusnid

	988.7.17 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}

	1016.1.1 = {
		add_matrilineal_spouse = corvurian_0016 #Ardor, Azilina’s husband
	}
	
	995.1.1 = {
		effect = {
			learn_language = language_damerian_common
		}
	}
	995.1.1 = {
		effect = {
			learn_language = language_korbarid
		}
	}
}

corvurian_0016 = {
	name = "Ardor"
	dna = dna_ardor_azilina
	religion = "castanorian_pantheon"
	culture = "moon_elvish"

	trait = education_diplomacy_1
	trait = race_elf
	trait = trusting
	trait = content
	trait = eccentric
	trait = intellect_bad_1

	854.8.30 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
}

corvurian_0017 = {
	name = "Castin"
	dynasty = dynasty_sil_mendruta	#síl Mendruta
	religion = "castanorian_pantheon"
	culture = "corvurian"
	
	trait = race_half_elf
	
	father = corvurian_0016 #Ardor
	mother = corvurian_0015 #Azilina síl Mendruta

	1020.2.26 = {
		birth = yes
	}
}

corvurian_0018 = {
	name = "RaT_ibidan"
	dynasty = dynasty_mendrutanid	#Mendrutanid
	religion = "korbarid_dragon_cult"
	culture = "korbarid"
	
	trait = race_human
	trait = education_martial_3
	trait = vengeful
	trait = stubborn
	trait = arbitrary

	966.1.4 = {
		birth = yes
	}
	
	995.1.1 = {
		add_spouse = corvurian_0019 #Zirașes Gujirusnid
	}
	
	1010.7.17 = {
		death = {
				death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

corvurian_0019 = {
	name = "ZiraS_es"
	dynasty = dynasty_gujirusnid	#Gujirusnid
	religion = "castanorian_pantheon"
	culture = "korbarid"
	female = yes
	
	trait = race_human
	trait = education_stewardship_2
	trait = humble
	trait = cynical
	trait = temperate

	970.8.30 = {
		birth = yes
	}
}
