#k_imuluses
##d_imuluses
###c_birsartansbar
549 = {		#Birsartansbar

    # Misc
    culture = bahari
    religion = rite_of_birsartan
	holding = castle_holding

    # History
}
2865 = {    #Eduz-Birsartan

    # Misc
    holding = church_holding

    # History

}
2866 = {    #Amaredaren

    # Misc
    holding = none

    # History

}

###c_eonkavan
551 = {		#Eonkavan

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = castle_holding

    # History
}

6356 = {		#Irsaghar

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = city_holding

    # History
}

6357 = {		#Arenbahar

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = none

    # History
}

###c_tunkalem
553 = {		#Tunkalem

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = city_holding

    # History
}

6358 = {		#Nur-Rahnmar

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = none

    # History
}

6359 = {		#Eorlamod

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = castle_holding

    # History
}

###c_kaboustraz
552 = {		#Kaboustraz

    # Misc
    culture = kuzarami
    religion = cult_of_kuza
	holding = city_holding

    # History
}

6360 = {		#Abadpas

    # Misc
    culture = kuzarami
    religion = cult_of_kuza
	holding = city_holding

    # History
}

6361 = {		#Fuulmod

    # Misc
    culture = kuzarami
    religion = cult_of_kuza
	holding = none

    # History
}

6362 = {		#Uelkiltrum

    # Misc
    culture = kuzarami
    religion = cult_of_kuza
	holding = castle_holding

    # History
}

##d_sad_kuz
###c_sad_kuz
556 = {		#Sad Kuz

    # Misc
    culture = kuzarami
    religion = cult_of_kuza
	holding = castle_holding

    # History
}

6363 = {		#Tarwassar

    # Misc
    culture = kuzarami
    religion = cult_of_kuza
	holding = city_holding

    # History
}

6364 = {		#Subtuhitu

    # Misc
    culture = kuzarami
    religion = cult_of_kuza
	holding = none

    # History
}

###c_kuztanuz
555 = {		#Kuztanuz

    # Misc
    culture = kuzarami
    religion = cult_of_kuza
	holding = castle_holding

    # History
}

6368 = {		#Eduz-Satkuza

    # Misc
    culture = kuzarami
    religion = cult_of_kuza
	holding = church_holding

    # History
}

6369 = {		#Arisah

    # Misc
    culture = kuzarami
    religion = cult_of_kuza
	holding = none

    # History
}

###c_tuklum_elum
554 = {		#Tuklum Elum

    # Misc
    culture = kuzarami
    religion = cult_of_kuza
	holding = castle_holding

    # History
}

6370 = {		#Wassa

    # Misc
    culture = kuzarami
    religion = cult_of_kuza
	holding = city_holding

    # History
}

6371 = {		#Kurwan

    # Misc
    culture = kuzarami
    religion = cult_of_kuza
	holding = none

    # History
}

###c_barzilsad
557 = {		#Barzilsad

    # Misc
    culture = kuzarami
    religion = cult_of_kuza
	holding = castle_holding

    # History
}

6365 = {		#Astuqir

    # Misc
    culture = kuzarami
    religion = cult_of_kuza
	holding = none

    # History
}

6366 = {		#Hapihawi

    # Misc
    culture = kuzarami
    religion = cult_of_kuza
	holding = city_holding

    # History
}

6367 = {		#Parnanas

    # Misc
    culture = kuzarami
    religion = cult_of_kuza
	holding = church_holding

    # History
}