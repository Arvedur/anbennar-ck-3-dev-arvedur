#k_beepeck
##d_beepeck
###c_beepeck
151 = {		#Beepeck

    # Misc
    culture = beefoot_halfling
    religion = small_temple
	holding = city_holding

    # History
}
1211 = {	#Hiveshold

    # Misc
    holding = castle_holding

    # History

}
1989 = {

    # Misc
    holding = church_holding

    # History

}
1990 = {

    # Misc
    holding = city_holding

    # History

}

###c_saltmarsh
155 = {		#West Saltfort

    # Misc
    culture = beefoot_halfling
    religion = small_temple
	holding = castle_holding

    # History
}
60 = {		#East Saltfort

    # Misc
    holding = none

    # History
}

###c_orston
157 = {		#Orston

    # Misc
    culture = beefoot_halfling
    religion = small_temple
	holding = castle_holding

    # History
}
1183 = {

    # Misc
    holding = city_holding

    # History

}
1184 = {

    # Misc
    holding = none

    # History

}

###c_the_strip
205 = {		#Brayden's Farm

    # Misc
    culture = beefoot_halfling
    religion = small_temple
	holding = castle_holding

    # History
}
1185 = {	#The Strip

    # Misc
    holding = none

    # History

}
1209 = {

    # Misc
    holding = none

    # History

}
1210 = {	#Pigsty

    # Misc
    holding = city_holding

    # History

}
